module Api
  module V1
    class TokensController < ApplicationController
      def index
        @tokens = Token.all
        render json: @tokens
      end

      def show
        render json: @token
      end

      def create
        @token = Token.new(token_params)

        if @token.save
          render json: @token, status: :created
        else
          render json: @token.errors, status: :unprocessable_entity
        end
      end

      def update
        if @token.update(token_params)
          render json: @token
        else
          render json: @token.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @token.destroy
      end

      private
        def set_token
          @token = Token.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def token_params
          params.require(:token).permit(:uid, :email, :token, :expires_at)
        end
    end
  end
end
