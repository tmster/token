class Token
  include Mongoid::Document
  include Mongoid::Timestamps

  field :email, type: String
  field :uid, type: Integer
  field :token, type: String
  field :expires_at, type: Time

  validates_presence_of :email
  validates_presence_of :uid
  validates_presence_of :token
  validates_uniqueness_of :uid, scope: :email

end
